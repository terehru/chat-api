INSTALLATION
------------

### Install via Composer

~~~
composer global require "fxp/composer-asset-plugin:^1.3.1"
~~~

~~~
# Опционально
composer global require hirak/prestissimo
~~~

~~~
composer update
~~~

### Пример конфигурации Nginx
~~~
server {
    charset utf-8;
    
    listen 80;

    server_name chat.loc;
    root        /var/www/chat/basic/web;
    index       index.php;

    
    location / {
        try_files $uri $uri/ /index.php$is_args$args;
    }

    location ~ ^/assets/.*\.php$ {
        deny all;
    }

    location ~ \.php$ {
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_pass 127.0.0.1:9000;
        # fastcgi_pass unix:/var/run/php5-fpm.sock;
            
        fastcgi_buffers 16 16k;
        fastcgi_buffer_size 16k;
        
        try_files $uri =404;
    }

    location ~* /\. {
        deny all;
    }
}
~~~

### Дамп БД
~~~
chat.sql
~~~



### Конфигурация БД
~~~
# Отредактировать файл
basic/config/db.php
~~~


Тестовые запросы
-------
### Список моделей
~~~
http://chat.loc/girls
~~~

### Конкретная модель
~~~
http://chat.loc/girls/1
~~~

### Установить новую цель
~~~
http://chat.loc/girls/1/goal/100500
http://chat.loc/girls/1/goal/
~~~

### Список сообщений (для конкретного пользователя)
~~~
http://chat.loc/messages/1?user_id=1
http://chat.loc/messages/1?user_id=2
~~~

### Отправка публичного сообщения
~~~
http://chat.loc/messages/send?user_id=1&girl_id=1&type=1&message=eeeeeha!
http://chat.loc/messages/send?user_id=2&girl_id=1&type=1&message=eeeeeha!
~~~

### Отправка приватного сообщения
~~~
http://chat.loc/messages/send?user_id=1&girl_id=1&type=2&message=eeeeeha!!!!
http://chat.loc/messages/send?user_id=2&girl_id=1&type=2&message=eeeeeha!!!!
~~~

### Отправка чаевых без сообщения
~~~
http://chat.loc/messages/send?user_id=1&girl_id=1&type=3&value=17
http://chat.loc/messages/send?user_id=2&girl_id=1&type=3&value=11
~~~

### Отправка чаевых c публичным сообщением
~~~
http://chat.loc/messages/send?user_id=1&girl_id=1&type=4&value=25&message=piu-piu
http://chat.loc/messages/send?user_id=1&girl_id=2&type=4&value=27&message=piu-piu
~~~

### Отправка чаевых c приватным сообщением
~~~
http://chat.loc/messages/send?user_id=1&girl_id=1&type=5&value=86&message=ping
http://chat.loc/messages/send?user_id=1&girl_id=2&type=5&value=92&message=pong
~~~

Логи
-------
### Broadcast
~~~
basic/web/broadcast.log
# Или
http://chat.loc/broadcast.log
~~~

Пояснения
-------
* И сообщения, и чаевые объединены в одну сущность
* Таблица messages денормализована преднамеренно
* В метод Comet::notify() добавлен параметр с ID модели
* Логи псевдо-бродкаста пишутся в файл
* Кешировать в данной реализации особо нечего (можно похитрить, но займет время), закеширован только метод с получением сообщений
* Количество сообщений "на старте" у пользователей может отличаться, если в последних сообщениях присутствуют приватные сообщения без чаевых
* Часть логики следует перенести в модели, но не стал терять на это время
* Yii использовал в основном для AR, сторонних библиотек для роутинга/фильтрации нет 
* Нет фильтрации типов запросов
* Тесты не писал, ход мысли, думаю, понятен
* Для заметного уменьшения нагрузки большую часть кода, которая получает данные через AR, нужно переписать, но в задании речь шла про AR


