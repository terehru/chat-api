<?php

Yii::$classMap['app\controllers\ChatController'] = '@app/components/ChatController.php';
Yii::$classMap['app\helpers\Comet'] = '@app/components/Comet.php';

return [
    'adminEmail' => 'admin@example.com',
];
