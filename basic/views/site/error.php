<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error" style="width: 800px; padding: 20px;">

    <h1 style="padding: 20px;">Something goes wrong :(</h1>

    <h3><?= $exception->getMessage() ?></h3>
    <pre><?= $exception->getTraceAsString() ?></pre>
</div>
