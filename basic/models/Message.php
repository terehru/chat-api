<?php

namespace app\models;

use app\helpers\Comet;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "messages".
 * @property integer $id
 * @property integer $user_id
 * @property integer $girl_id
 * @property integer $type
 * @property integer $transaction_id
 * @property string $message
 * @property integer $value
 * @property string $created
 * @property Girl $girl
 * @property User $user
 * @property Transaction $transaction
 */
class Message extends \yii\db\ActiveRecord
{
    const TYPE_MESSAGE_PUBLIC       = 1;
    const TYPE_MESSAGE_PRIVATE      = 2;
    const TYPE_TIPS                 = 3;
    const TYPE_MESSAGE_TIPS_PUBLIC  = 4;
    const TYPE_MESSAGE_TIPS_PRIVATE = 5;

    /**
     * Behaviours
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
                'value'      => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'messages';
    }

    /**
     * Before validation
     * @return bool
     */
    public function beforeValidate()
    {
        // Message is mandatory
        if ($this->type != self::TYPE_TIPS && !$this->message) {
            return false;
        }

        // Value is mandatory
        if (in_array(
                $this->type,
                [self::TYPE_TIPS, self::TYPE_MESSAGE_TIPS_PUBLIC, self::TYPE_MESSAGE_TIPS_PRIVATE]
            ) && !$this->value) {
            return false;
        }

        // Message w/o tips
        if (!in_array(
                $this->type,
                [self::TYPE_TIPS, self::TYPE_MESSAGE_TIPS_PUBLIC, self::TYPE_MESSAGE_TIPS_PRIVATE]
            )
            && !$this->transaction) {
            $this->value = null;
        }

        return parent::beforeValidate();
    }

    /**
     * After save
     *
     * @param bool $insert
     * @param array $changedAttributes
     *
     * @throws \Exception
     */
    public function afterSave($insert, $changedAttributes)
    {
        // Message with tips
        if (in_array(
                $this->type,
                [self::TYPE_TIPS, self::TYPE_MESSAGE_TIPS_PUBLIC, self::TYPE_MESSAGE_TIPS_PRIVATE]
            ) && !$this->transaction) {

            if (!$this->user->changeBalance(-$this->value)) {
                throw new \Exception("Can't change user balance");
            }

            if (!$this->girl->changeBalance($this->value)) {
                throw new \Exception("Can't change girl balance");
            }

            $transaction = new Transaction();
            $transaction->girl_id = $this->girl_id;
            $transaction->user_id = $this->user_id;
            $transaction->value = $this->value;
            if (!$transaction->save()) {
                $this->delete();
                throw new \Exception("Can't create transaction");
            }

            if (!$this->girl->updateTopSponsor()) {
                throw new \Exception("Can't update girl top sponsor");
            }
        }

        $this->refresh();

        if (in_array($this->type, [self::TYPE_MESSAGE_PUBLIC, self::TYPE_TIPS, self::TYPE_MESSAGE_TIPS_PUBLIC])) {
            Comet::notify('message', $this->getAttributes(), $this->girl->subscribersIds, $this->girl->id);
        } elseif (in_array($this->type, [self::TYPE_MESSAGE_PRIVATE])) {
            Comet::notify('message', $this->getAttributes(), [$this->user_id], $this->girl->id);
        } elseif (in_array($this->type, [self::TYPE_MESSAGE_TIPS_PRIVATE])) {
            $payload = $this->getAttributes();
            $payload['message'] = false;

            $subscribers = $this->girl->subscribersIds;
            if (($key = array_search($this->user->id, $subscribers)) !== false) {
                unset($subscribers[$key]);
            }
            Comet::notify('message', $payload, $subscribers, null);
            Comet::notify('message', $this->getAttributes(), $this->user->id, $this->girl->id);
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'girl_id', 'type', 'transaction_id', 'value'], 'integer'],
            [['type'], 'required'],
            [['message'], 'string'],
            [['created'], 'safe'],
            [
                ['girl_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Girl::className(),
                'targetAttribute' => ['girl_id' => 'id'],
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['user_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'             => 'ID',
            'user_id'        => 'User ID',
            'girl_id'        => 'Girl ID',
            'type'           => 'Type',
            'transaction_id' => 'Transaction ID',
            'message'        => 'Message',
            'value'          => 'Value',
            'created'        => 'Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGirl()
    {
        return $this->hasOne(Girl::className(), ['id' => 'girl_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transaction::className(), ['id' => 'transaction_id']);
    }
}
