<?php

namespace app\models;

use app\helpers\Comet;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "girls".
 * @property integer $id
 * @property string $name
 * @property integer $goal
 * @property integer $progress
 * @property integer $top_user_id
 * @property string $created
 * @property User[] $topUser
 * @property Message[] $messages
 * @property Transaction[] $transactions
 * @property array $subscribersIds
 */
class Girl extends \yii\db\ActiveRecord
{
    /**
     * Behaviours
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
                'value'      => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'girls';
    }

    /**
     * Change balance
     *
     * @param integer $value
     *
     * @return bool
     */
    public function changeBalance($value)
    {
        $this->progress += $value;

        return $this->save();
    }

    /**
     * Update top sponsor
     * @return bool
     */
    public function updateTopSponsor()
    {
        $topSponsorId = (new \yii\db\Query())
            ->select('user_id')
            ->from('transactions')
            ->andWhere(['girl_id' => $this->id])
            ->andWhere('created > (NOW() - INTERVAL 3 HOUR)')
            ->orderBy('SUM(value) DESC')
            ->groupBy('user_id')
            ->scalar();

        if ($topSponsorId != $this->top_user_id) {
            $newSponsor = User::findOne($topSponsorId);
            Comet::notify('sponsor', [$newSponsor->getAttributes(['id', 'name'])], $this->subscribersIds, $this->id);
        }

        $this->top_user_id = $topSponsorId;

        return $this->save();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['goal', 'progress', 'top_user_id'], 'integer'],
            [['created'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'       => 'ID',
            'name'     => 'Name',
            'goal'     => 'Goal',
            'progress' => 'Progress',
            'created'  => 'Created',
        ];
    }

    /**
     * Get all subscribers ids
     * @return array
     */
    public function getSubscribersIds()
    {
        return User::find()->select('id')->column();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['girl_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopUser()
    {
        return $this->hasOne(User::className(), ['id' => 'top_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['girl_id' => 'id']);
    }
}
