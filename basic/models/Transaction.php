<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "transactions".
 * @property integer $id
 * @property integer $user_id
 * @property integer $girl_id
 * @property integer $value
 * @property string $created
 * @property Girl $girl
 * @property User $user
 */
class Transaction extends \yii\db\ActiveRecord
{
    /**
     * Behaviours
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
                'value'      => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transactions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'girl_id', 'value'], 'integer'],
            [['value'], 'required'],
            [['created'], 'safe'],
            [
                ['girl_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Girl::className(),
                'targetAttribute' => ['girl_id' => 'id'],
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['user_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'      => 'ID',
            'user_id' => 'User ID',
            'girl_id' => 'Girl ID',
            'value'   => 'Value',
            'created' => 'Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGirl()
    {
        return $this->hasOne(Girl::className(), ['id' => 'girl_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
