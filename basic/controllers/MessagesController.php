<?php

namespace app\controllers;

use app\models\Girl;
use app\models\Message;
use app\models\User;

class MessagesController extends ChatController
{
    /**
     * Last messages list
     *
     * @param $girlId
     */
    public function actionIndex($girlId)
    {
        $userId = \Yii::$app->request->get('user_id');

        list($data, $girlData) = $this->getMessages($girlId, $userId);

        $this->output($data, $girlData);
    }

    /**
     * Get messager for specific girl and user
     *
     * @param integer $girlId
     * @param integer $userId
     *
     * @return array
     */
    protected function getMessages($girlId, $userId)
    {
        $cacheKey = "messages-{$girlId}-{$userId}";
        $cacheMessages = \Yii::$app->cache->get($cacheKey);
        if ($cacheMessages === false) {
            $messages = Message::find()->with('user')
                ->where('girl_id = :girl_id', [':girl_id' => $girlId])
                ->orderBy('id DESC')
                ->limit(10)
                ->all();

            $cacheMessages = $messages;
            \Yii::$app->cache->set($cacheKey, $cacheMessages, 4);
        }

        $girl = Girl::findOne($girlId);
        if (!$girl) {
            $this->error("Girl not found", "girl_not_found_1", 404);
        }

        $user = User::findOne($userId);
        if (!$user) {
            $this->error("User not found", "user_not_found_1", 404);
        }

        $result = [];
        foreach ($cacheMessages as $message) {
            if ($message->type == Message::TYPE_MESSAGE_PRIVATE && $message->user_id != $user->id) {
                continue;
            }

            if ($message->type == Message::TYPE_MESSAGE_TIPS_PRIVATE && $message->user_id != $user->id) {
                $message->message = null;
            }

            $temp = $message->attributes;
            $temp['name'] = $message->user->name;
            $result[] = $temp;
        }

        $result = array_reverse($result);

        $girlData = $girl->getAttributes(null, ['created', 'top_user_id']);
        $girlData['topUser'] = [
            'id'   => $girl->top_user_id,
            'name' => $girl->topUser->name,
        ];

        return [$result, $girlData];
    }

    /**
     * Send new message/tips
     */
    public function actionSend()
    {
        $girlId = \Yii::$app->request->get('girl_id');
        $userId = \Yii::$app->request->get('user_id');
        $type = \Yii::$app->request->get('type');
        $text = \Yii::$app->request->get('message');
        $value = \Yii::$app->request->get('value');

        $user = User::findOne($userId);
        if (!$user) {
            $this->error("User not found", "user_not_found_2", 404);
        }

        $girl = Girl::findOne($girlId);
        if (!$girl) {
            $this->error("Girl not found", "girl_not_found_2", 404);
        }

        if ($type === null) {
            $this->error("Type not specified", "no_type", 400);
        }

        $message = new Message();
        $message->girl_id = $girlId;
        $message->user_id = $userId;
        $message->message = $text;
        $message->type = $type;
        $message->value = $value;

        if (!$message->save()) {
            $this->error("Can't save message", 'message_not_saved', 400);
        }

        $this->output($message->getAttributes(null, ['created']));
    }
}
