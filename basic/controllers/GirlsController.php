<?php

namespace app\controllers;

use app\models\Girl;

class GirlsController extends ChatController
{
    /**
     * Before action
     * @param \yii\base\Action $action
     *
     * @return bool
     */
    public function beforeAction($action)
    {
        return parent::beforeAction($action);
    }

    /**
     * Girls list
     */
    public function actionIndex()
    {
        $girls = Girl::find()->all();

        $output = [];
        foreach ($girls as $girl) {
            $output[] = $girl->getAttributes(null, ['created']);
        }

        $this->output($output);
    }

    /**
     * One girl
     *
     * @param integer $id
     */
    public function actionGirl($id)
    {
        $girl = Girl::findOne($id);

        $this->output($girl->getAttributes(null, ['created']));
    }

    /**
     * Make new goal
     *
     * @param $id
     * @param $goal
     */
    public function actionGoal($id, $goal = null)
    {
        $girl = Girl::findOne($id);

        if (!$girl) {
            $this->error("Girl not found", "girl_not_found", 404);
        }

        $girl->goal = $goal;
        $girl->progress = 0;

        if (!$girl->save()) {
            $this->error("Can't save new goal", "goal_not_saved", 400);
        }

        $this->output([
            'goal' => is_numeric($girl->goal) ? (int)$girl->goal : null,
            'progress' => $girl->progress
        ]);
    }
}
