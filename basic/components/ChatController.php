<?php

namespace app\controllers;

class ChatController extends \yii\web\Controller
{
    /**
     * Headers
     */
    public function headers()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    }

    /**
     * Output
     *
     * @param array $data
     * @param array|null $metadata
     * @param bool $exit
     */
    public function output($data, $metadata = null, $exit = true)
    {
        $this->headers();

        if ($metadata == null || !is_array($metadata)) {
            $metadata = [];
        }
        $status = ['status' => 'success', 'error' => null];
        $metadata = array_merge($status, $metadata);
        $output = json_encode(
            [
                'data'     => $data,
                'metadata' => $metadata,
            ],
            JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE
        );

        echo $output;

        if ($exit) {
            \Yii::$app->end();
        }
    }

    /**
     * Error
     *
     * @param string $string
     * @param string|null $code
     * @param integer|null $status_code
     * @param bool $exit
     */
    public function error($string, $code = null, $status_code = null, $exit = true)
    {
        $this->headers();

        if ($code == null) {
            $code = $string;
        }

        if ($status_code !== null) {
            http_response_code($status_code);
        }

        echo json_encode(
            [
                'data'     => null,
                'metadata' => [
                    'status' => 'error',
                    'error'  => [
                        'code'    => $code,
                        'message' => $string,
                    ],
                ],
            ],
            JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE
        );

        if ($exit) {
            \Yii::$app->end();
        }
    }
}