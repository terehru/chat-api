<?php

namespace app\helpers;

class Comet
{
    /**
     * Notify users and girl
     *
     * @param string $event
     * @param array $payload
     * @param array $usersIds
     * @param integer $girlId
     */
    public static function notify($event, $payload, $usersIds, $girlId)
    {
        $data = [
            'event'    => $event,
            'payload'  => $payload,
            'usersIds' => $usersIds,
            'girlId'   => $girlId,
        ];
        file_put_contents(
            \Yii::getAlias('@app/web') . '/broadcast.log',
            var_export($data, true) . PHP_EOL . '------' . PHP_EOL,
            FILE_APPEND
        );
    }
}